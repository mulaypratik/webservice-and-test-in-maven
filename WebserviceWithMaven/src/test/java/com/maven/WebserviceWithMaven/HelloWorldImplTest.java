package com.maven.WebserviceWithMaven;

import com.javatpoint.HelloWorldImpl;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class HelloWorldImplTest extends TestCase {

	private HelloWorldImpl hwi;

	public HelloWorldImplTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(HelloWorldImplTest.class);
	}

	public void testGetHelloWorldAsString() {
		// fail("Not yet implemented");
	}

	public void testGetAddition() {
		hwi = new HelloWorldImpl();

		assertEquals(9, hwi.getAddition(3, 5));
	}

}
