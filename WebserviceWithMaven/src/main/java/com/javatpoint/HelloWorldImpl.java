package com.javatpoint;

import javax.jws.WebService;

import com.javatpoint.HelloWorld;

//Service Implementation
//@WebService(endpointInterface = "com.maven.WebserviceWithMaven.HelloWorld")
@WebService(endpointInterface = "com.javatpoint.HelloWorld")
public class HelloWorldImpl implements HelloWorld {

	public String getHelloWorldAsString(String name) {
		return "Hello World JAX-WS " + name;
	}

	public int getAddition(int no1, int no2) {
		return (no1 + no2);
	}

}