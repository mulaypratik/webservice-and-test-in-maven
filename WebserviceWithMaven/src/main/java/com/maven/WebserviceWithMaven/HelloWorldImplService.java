package com.maven.WebserviceWithMaven;

import javax.xml.ws.Endpoint;

import com.javatpoint.HelloWorldImpl;

public class HelloWorldImplService {
	public static void main(String[] args) {
		System.out.println("Hello World!");

		Endpoint.publish("http://localhost:7779/ws/hello", new HelloWorldImpl());

		System.out.println("Service started.");
	}
}
